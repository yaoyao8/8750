package CarSafetySystem::SecurityCase
public
	with security_trust;

	
annex Resolute {**
	--
	-- Rule check_security checks that the data component on sending side of a logical
	--connection has a trustlevel equal to or greater than that of the data
	--component of the receiving component.
	--
	check_security(self:component) <=
	** "Confidentality security level is met on " self **
	forall (c1 : component) (conn : connection) (c2 : component) . (connected
	(c1, conn, c2)) and (has_property (c1, Security_Trust::TrustLevel)) and
	(has_property (c2, Security_Trust::TrustLevel))
	=>
	property (c1, Security_Trust::TrustLevel) <= property (c2,
	Security_Trust::TrustLevel)
	--
	-- Rule check_membership checks if the components on either side of the logical
	--connection have the same Group Membership
	--
	check_membership(self:component) <=
	** "Group membership criteria is met " self **
	forall (c1 : component) (conn : connection) (c2 : component) . (connected
	(c1, conn, c2)) and (has_property (c1, Security_Trust::GroupMembership)) and
	(has_property (c2, Security_Trust::GroupMembership))
	=>
	property (c1, Security_Trust::GroupMembership) = property (c2,
	Security_Trust::GroupMembership)
	--
	-- Rule check_composite is the logical and of security rules check_security and check_membership e.g. that
	--each component belongs to the same group and the data confidenatlity is
	--maintained between the components
	--
	check_composite(self:component) <=
	** "Composite Check" self **
	check_security(self) and check_membership(self)
	**};
end CarSafetySystem::SecurityCase;